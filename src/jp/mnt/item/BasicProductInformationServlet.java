package jp.mnt.item;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.DataSourceManager;
import jp.mnt.dao.ItemDAO;
import jp.mnt.dto.ItemDTO;
import jp.mnt.dto.SearchDTO;

/**
 * Servlet implementation class BasicProductInformationServlet
 */
@WebServlet("/basicProductInformation")
public class BasicProductInformationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.sendRedirect("login.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(false);
		if (session == null) {
			doGet(request, response);
			return;
		}
		if ("".equals(session.getAttribute("staff_id"))) {
			doGet(request, response);
			return;
		}
		request.setCharacterEncoding("UTF-8");
		String searchString = request.getParameter("item");
		String category = request.getParameter("category");
		String genre = request.getParameter("genre");
		

		// 文字が入力されなかったとき商品を表示しない
		String message = "";
		if ("".equals(searchString)) {
			message = "タイトルを入力してください";
			request.setAttribute("message", message);
			request.getRequestDispatcher("/WEB-INF/jsp/basicProductInformation.jsp").forward(request, response);
			return;
		}
		
		SearchDTO searchList = new SearchDTO();
		searchList.setSearchString(searchString);
		searchList.setCategory(category);
		searchList.setGenre(genre);
		session.setAttribute("searchList", searchList);
		

		// コネクションの取得
		// listにDAOで取ったデータを保持
		Connection conn = null;
		ArrayList<ItemDTO> list;
		int count = 0;
		try {
			conn = DataSourceManager.getConnection();
			ItemDAO dao = new ItemDAO(conn);

			// カテゴリ、ジャンルが選択されているか判定
			if (category.equals("0")) {
				list = dao.selectText(searchString);
				count = dao.selectTextCount(searchString);
			} else if (genre.equals("0")) {
				list = dao.selectTextAndCategory(searchString, category);
				count = dao.selectTextCategoryCount(searchString, category);
			} else {
				list = dao.selectTextAndCategoryAndGenre(searchString, category, genre);
				count = dao.selectTextCategoryAndGenreCount(searchString, category, genre);
			}
			//見つかった件数を送る
			request.setAttribute("ItemCount", count);
			//見つかった情報をリストで送る
			request.setAttribute("ItemList", list);
			message = "";
			if (list.size() == 0) {
				message = "検索文字列に該当するデータは存在しません";
			}
			request.setAttribute("message", message);
			request.getRequestDispatcher("/WEB-INF/jsp/basicProductInformation.jsp").forward(request, response);

		} catch (SQLException e) {

			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (NamingException e) {

			e.printStackTrace();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

}
