package jp.mnt.item;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.DataSourceManager;
import jp.mnt.dao.ItemDAO;
import jp.mnt.dao.RecommendedDAO;
import jp.mnt.dto.ItemDTO;
import jp.mnt.dto.SearchDTO;

/**
 * Servlet implementation class RecommendedServlet
 */
@WebServlet("/recommended")
public class RecommendedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.sendRedirect("login.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(false);
		if (session == null) {
			doGet(request, response);
			return;
		}
		if ("".equals(session.getAttribute("staff_id"))) {
			doGet(request, response);
			return;
		}

		String itemId = request.getParameter("i");
		String recommended = request.getParameter("r");
		SearchDTO searchList = (SearchDTO) session.getAttribute("searchList");
		String searchString = searchList.getSearchString();
		String category = searchList.getCategory();
		String genre = searchList.getGenre();
		

		Connection conn = null;
		ArrayList<ItemDTO> list;
		String message;
		try {

			conn = DataSourceManager.getConnection();
			RecommendedDAO rDao = new RecommendedDAO(conn);

			ItemDAO dao = new ItemDAO(conn);

			if ("1".equals(recommended)) {
				rDao.RecommendedEdit(Integer.parseInt(itemId), 0);
			} else {
				rDao.RecommendedEdit(Integer.parseInt(itemId), 1);
			}

			// カテゴリ、ジャンルが選択されているか判定
			if (category.equals("0")) {
				list = dao.selectText(searchString);
			} else if (genre.equals("0")) {
				list = dao.selectTextAndCategory(searchString, category);
			} else {
				list = dao.selectTextAndCategoryAndGenre(searchString, category, genre);
			}

			request.setAttribute("ItemList", list);
			message = "";
			if (list.size() == 0) {
				message = "検索文字列に該当するデータは存在しません";
			}
			request.setAttribute("message", message);

			request.getRequestDispatcher("/WEB-INF/jsp/basicProductInformation.jsp").forward(request, response);

		} catch (SQLException e) {

			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (NamingException e) {

			e.printStackTrace();
		}

	}

}
