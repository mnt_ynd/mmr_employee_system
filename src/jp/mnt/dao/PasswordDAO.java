package jp.mnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.naming.NamingException;

public class PasswordDAO {
	Connection conn = null;

	public PasswordDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	public void updatePassword(String id, String pass)  throws SQLException, NamingException, Exception{

		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        STAFF");
		sb.append("    SET");
		sb.append("        PASSWORD = PASSWORD(?)");
		sb.append("    WHERE");
		sb.append("        STAFF_ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			ps.setString(1, pass);
			ps.setInt(2, Integer.parseInt(id));

			ps.executeUpdate();

		} catch (SQLException e) {
			throw e;
		} catch (NullPointerException e) {
			throw e;
		}
	}

}
