package jp.mnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.mnt.dto.StaffDTO;

/**
 * スタッフマスタ用DAO
 * 
 * @author ソフトブレーン富岡
 *
 */
public class StaffDAO {
	Connection conn = null;

	public StaffDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	public ArrayList<StaffDTO> selectAll() throws SQLException {

		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        STAFF_ID");
		sb.append("        ,LAST_NAME");
		sb.append("        ,FIRST_NAME");
		sb.append("    FROM");
		sb.append("        STAFF;");

		// リストの作成
		ArrayList<StaffDTO> list = new ArrayList<StaffDTO>();
		// SQL文実行
		PreparedStatement ps = conn.prepareStatement(sb.toString());
		ResultSet rs = ps.executeQuery();

		// SQLの結果を取得し、リストに詰める
		while (rs.next()) {
			StaffDTO empRowData = new StaffDTO();
			empRowData.setStaffId(rs.getInt("STAFF_ID"));
			empRowData.setFirstName(rs.getString("FIRST_NAME"));
			empRowData.setLastName(rs.getString("LAST_NAME"));
			list.add(empRowData);
		}

		return list;
	}

	/**
	 * 指定のID、パスワードでログインできるかを判定するメソッド
	 * 
	 * @author ソフトブレーン株式会社 富岡
	 * @param id
	 *            ログインID
	 * @param password
	 *            パスワード
	 * @return ログイン可能ならtrue
	 * @throws SQLException
	 */
	public boolean selectByIdAndPassword(String id, String password) throws SQLException {

		// SQL文の作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUNT(*)");
		sb.append("    FROM");
		sb.append("        STAFF");
		sb.append("    WHERE");
		sb.append("        STAFF_ID = ?");
		sb.append("        AND PASSWORD = PASSWORD(?)");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// ?部分に値をセットして検索
			ps.setString(1, id);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();

			// 結果が存在しているならtrue
			if (rs.next()) {
				return rs.getInt(1) == 1;
			}

			return false;

		} catch (SQLException e) {
			throw e;
		}
	}

}
