package jp.mnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RecommendedDAO {
	Connection conn = null;

	public RecommendedDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	public void RecommendedEdit(int itemId, int recommended) throws SQLException{

		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        ITEM");
		sb.append("    SET");
		sb.append("        RECOMMENDED_FLG = ?");
		sb.append("    WHERE");
		sb.append("        ITEM_ID = ?");
		
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			
			ps.setInt(1, recommended);
			ps.setInt(2, itemId);;

			ps.executeUpdate();
			

		} catch (SQLException e) {
			throw e;
		}catch (NullPointerException e) {
			
			throw e;
		}
	}
}
