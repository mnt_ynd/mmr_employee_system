package jp.mnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RentalDAO {
	Connection conn = null;

	public RentalDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	/**
	 * レンタル詳細に識別番号をセットする
	 * 
	 * @param rentalDetailNumber
	 * @param identificationNumber
	 * @return
	 */
	public int setIdentificationNumBer(int rentalDetailNumber, int identificationNumber) {

		// SQL文
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        rental_detail");
		sb.append("    SET");
		sb.append("        IDENTIFICATION_NUMBER = ?");
		sb.append("    WHERE");
		sb.append("        RENTAL_DETAIL_NUMBER = ?;");

		try {
			// SQL文実行
			PreparedStatement ps = conn.prepareStatement(sb.toString());
			ps.setInt(1, identificationNumber);
			ps.setInt(2, rentalDetailNumber);

			// 結果
			return ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	/**
	 * 受付番号とスタッフIDで返却処理を行う
	 * 
	 * @param returnStaffId
	 * @param rentalNumber
	 * @return
	 */
	public int doRental(int rentalStaffId, int rentalNumber) {

		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        rental");
		sb.append("    SET");
		sb.append("        status_id = 2");
		sb.append("        ,rental_staff_id = ?");
		sb.append("        ,to_return_datetime = date_add(");
		sb.append("            now()");
		sb.append("            ,INTERVAL 7 day");
		sb.append("        )");
		sb.append("    WHERE");
		sb.append("        rental_number = ?;");

		try {
			// SQL文実行
			PreparedStatement ps = conn.prepareStatement(sb.toString());
			ps.setInt(1, rentalStaffId);
			ps.setInt(2, rentalNumber);

			// 結果
			return ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	/**
	 * 受付番号とスタッフIDで返却処理を行う
	 * 
	 * @param returnStaffId
	 * @param rentalNumber
	 * @return
	 */
	public int doReturn(int returnStaffId, int rentalNumber) {

		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        rental");
		sb.append("    SET");
		sb.append("        status_id = 3");
		sb.append("        ,return_staff_id = ?");
		sb.append("        ,return_datetime = now()");
		sb.append("    WHERE");
		sb.append("        rental_number = ?;");

		try {
			// SQL文実行
			PreparedStatement ps = conn.prepareStatement(sb.toString());
			ps.setInt(1, returnStaffId);
			ps.setInt(2, rentalNumber);

			// 結果
			return ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	/**
	 * 受付番号を渡すと、レンタルする商品点数を検索する
	 * 
	 * @param rentalNumber
	 * @return
	 */
	public int countContents(int rentalNumber) {
		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUNT(*) AS RENTAL_CONTENTS");
		sb.append("    FROM");
		sb.append("        rental_detail");
		sb.append("    WHERE");
		sb.append("        rental_number = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// ?に値をセットして実行
			ps.setInt(1, rentalNumber);

			// SQL文実行
			ResultSet rs = ps.executeQuery();

			int count = 0;

			// SQLの結果を取得し、リストに詰める
			while (rs.next()) {
				count = rs.getInt("COUNT_CONTENTS");
			}

			return count;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * レンタル一覧を全件検索
	 * @return
	 */
	public int countRentalAll() {
		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUNT(*) AS COUNT");
		sb.append("    FROM");
		sb.append("        rental;");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// SQL文実行
			ResultSet rs = ps.executeQuery();

			int count = 0;

			// SQLの結果を取得し、リストに詰める
			while (rs.next()) {
				count = rs.getInt("COUNT");
			}

			return count;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
}
