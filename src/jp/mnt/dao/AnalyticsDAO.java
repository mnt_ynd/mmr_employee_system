package jp.mnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.NameNotFoundException;

public class AnalyticsDAO {
	Connection conn = null;

	public AnalyticsDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	public int[] thisYear() throws SQLException, NameNotFoundException {

		//SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("  case when ((year(curdate()) - year(birthday)) - (right(curdate(), 5) < right(birthday, 5))) < 20");
		sb.append("       then 10");
		sb.append("       when ((year(curdate()) - year(birthday)) - (right(curdate(), 5) < right(birthday, 5))) < 30");
		sb.append("       then 20");
		sb.append("       when ((year(curdate()) - year(birthday)) - (right(curdate(), 5) < right(birthday, 5))) < 40");
		sb.append("       then 30");
		sb.append("       when ((year(curdate()) - year(birthday)) - (right(curdate(), 5) < right(birthday, 5))) < 50");
		sb.append("       then 40");
		sb.append("       when ((year(curdate()) - year(birthday)) - (right(curdate(), 5) < right(birthday, 5))) >= 50");
		sb.append("       then 50");
		sb.append("  end as AGE");
		sb.append("        ,COUNT(*) AS COUNT");
		sb.append("    FROM");
		sb.append("        rental");
		sb.append("            LEFT JOIN USER");
		sb.append("                ON rental.USER_INDEX = user.USER_INDEX");
		sb.append("    WHERE");
		sb.append("        ORDER_DATETIME >= DATE_ADD(");
		sb.append("            DATE_ADD(LAST_DAY(NOW()),INTERVAL 1 DAY),INTERVAL -2 MONTH");
		sb.append("        )");
		sb.append("        AND ORDER_DATETIME < DATE_ADD(");
		sb.append("            DATE_ADD(LAST_DAY(NOW()) ,INTERVAL 1 DAY),INTERVAL -1 MONTH");
		sb.append("        )");
		sb.append("    GROUP BY");
		sb.append("        AGE;");


		// SQL文実行
		PreparedStatement ps = conn.prepareStatement(sb.toString());
		ResultSet rs = ps.executeQuery();

		int[] date = new int[7];

		// SQLの結果を取得し、リストに詰める
		int count = 0;
		while (rs.next()) {
			date[count] = rs.getInt("COUNT");
			count++;
		}

		return date;
	}

	public int[] lastYear() throws SQLException, NameNotFoundException {

		//SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("  case when ((year(curdate()) - year(birthday)) - (right(curdate(), 5) < right(birthday, 5))) < 20");
		sb.append("       then 10");
		sb.append("       when ((year(curdate()) - year(birthday)) - (right(curdate(), 5) < right(birthday, 5))) < 30");
		sb.append("       then 20");
		sb.append("       when ((year(curdate()) - year(birthday)) - (right(curdate(), 5) < right(birthday, 5))) < 40");
		sb.append("       then 30");
		sb.append("       when ((year(curdate()) - year(birthday)) - (right(curdate(), 5) < right(birthday, 5))) < 50");
		sb.append("       then 40");
		sb.append(
				"       when ((year(curdate()) - year(birthday)) - (right(curdate(), 5) < right(birthday, 5))) < 60");
		sb.append("       then 50");
		sb.append(
				"       when ((year(curdate()) - year(birthday)) - (right(curdate(), 5) < right(birthday, 5))) < 70");
		sb.append("       then 60");
		sb.append(
				"       when ((year(curdate()) - year(birthday)) - (right(curdate(), 5) < right(birthday, 5))) >= 70");
		sb.append("       then 70");
		sb.append("  end as AGE");
		sb.append("        ,COUNT(*) AS COUNT");
		sb.append("    FROM");
		sb.append("        rental");
		sb.append("            LEFT JOIN USER");
		sb.append("                ON rental.USER_INDEX = user.USER_INDEX");
		sb.append("    WHERE");
		sb.append(
				"        ORDER_DATETIME >= DATE_ADD(DATE_ADD(DATE_ADD(LAST_DAY(NOW()),INTERVAL 1 DAY),INTERVAL -2 MONTH),INTERVAL -1 year)");
		sb.append(
				"        AND ORDER_DATETIME < DATE_ADD(DATE_ADD(DATE_ADD(LAST_DAY(NOW()) ,INTERVAL 1 DAY),INTERVAL -1 MONTH),INTERVAL -1 year)");
		sb.append("    GROUP BY");
		sb.append("        AGE;");

		// SQL文実行
		PreparedStatement ps = conn.prepareStatement(sb.toString());
		ResultSet rs = ps.executeQuery();

		int[] date = new int[7];

		// SQLの結果を取得し、リストに詰める
		int count = 0;
		while (rs.next()) {
			date[count] = rs.getInt("COUNT");
			count++;
		}

		return date;
	}
}
