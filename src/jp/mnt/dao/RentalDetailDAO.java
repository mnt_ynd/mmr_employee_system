package jp.mnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.mnt.dto.RentalDetailDTO;

public class RentalDetailDAO {

	Connection conn = null;

	public RentalDetailDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	/**
	 * 受付番号から詳細内容をreturnする。
	 * 
	 * @author ソフトブレーン富岡
	 * @param 受付番号
	 * @return RentalDetailDTO
	 * @throws SQLException
	 */
	public ArrayList<RentalDetailDTO> selectDetailByRentalNumber(String rentalNumber) throws SQLException {

		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("     SELECT");
		sb.append("                rental.rental_number");
		sb.append("                ,rental.order_Datetime");
		sb.append("                ,date_add(");
		sb.append("                    ORDER_DATETIME");
		sb.append("                    ,INTERVAL 7 day");
		sb.append("                ) AS TO_RETURN_DATETIME");
		sb.append("                ,rental.return_datetime");
		sb.append("                ,CONCAT(s1.LAST_NAME, ' ', s1.FIRST_NAME) AS RENTAL_STAFF_NAME");
		sb.append("                ,CONCAT(s2.LAST_NAME, ' ', s2.FIRST_NAME) AS RETURN_STAFF_NAME");
		sb.append("                ,status_name");
		sb.append("                ,COUNT(*)");
		sb.append("            FROM");
		sb.append("                rental INNER JOIN rental_detail");
		sb.append("                    ON rental.rental_number = rental_detail.rental_number INNER JOIN status");
		sb.append("                    ON rental.status_id = status.status_id INNER JOIN staff AS s1");
		sb.append("                    ON rental.rental_staff_id = s1.staff_id INNER JOIN staff AS s2");
		sb.append("                    ON rental.return_staff_id = s2.staff_id");
		sb.append("        WHERE");
		sb.append("            rental.rental_number = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// ?に値をセットして実行
			ps.setString(1, rentalNumber);

			// リストの作成
			ArrayList<RentalDetailDTO> list = new ArrayList<RentalDetailDTO>();
			// SQL文実行
			ResultSet rs = ps.executeQuery();

			// SQLの結果を取得し、リストに詰める
			while (rs.next()) {
				RentalDetailDTO rentalRowData = new RentalDetailDTO();
				rentalRowData.setRentalNumber(rs.getInt("RENTAL_NUMBER"));
				rentalRowData.setOrderDatetime(rs.getTimestamp("ORDER_DATETIME"));
				rentalRowData.setToReturnDatetime(rs.getDate("TO_RETURN_DATETIME"));
				rentalRowData.setReturnDatetime(rs.getDate("RETURN_DATETIME"));
				rentalRowData.setCount(rs.getInt("COUNT"));
				rentalRowData.setRentalStaffName(rs.getString("RENTAL_STAFF_NAME"));
				rentalRowData.setReturnStaffName(rs.getString("RETURN_STAFF_NAME"));
				rentalRowData.setStatusName(rs.getString("STATUS_NAME"));
				list.add(rentalRowData);
			}

			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<RentalDetailDTO> selectContentsByRentalNumber(String rentalNumber) throws SQLException {
		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        IDENTIFICATION_NUMBER");
		sb.append("        ,rd1.ITEM_ID");
		sb.append("        ,ITEM_NAME");
		sb.append("        ,CATEGORY_NAME");
		sb.append("        ,GENRE_NAME");
		sb.append("        ,RENTAL_DETAIL_NUMBER");
		sb.append("    FROM");
		sb.append("        rental_detail AS rd1");
		sb.append("            LEFT JOIN item");
		sb.append("                ON rd1.ITEM_ID = item.ITEM_ID");
		sb.append("            LEFT JOIN category");
		sb.append("                ON item.CATEGORY_ID = category.CATEGORY_ID");
		sb.append("            LEFT JOIN genre");
		sb.append("                ON item.GENRE_ID = genre.GENRE_ID");
		sb.append("    WHERE");
		sb.append("        RENTAL_NUMBER = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// ?に値をセットして実行
			ps.setString(1, rentalNumber);

			// リストの作成
			ArrayList<RentalDetailDTO> list = new ArrayList<RentalDetailDTO>();
			// SQL文実行
			ResultSet rs = ps.executeQuery();

			// SQLの結果を取得し、リストに詰める
			while (rs.next()) {
				RentalDetailDTO rentalContentsRowData = new RentalDetailDTO();
				rentalContentsRowData.setCategoryName(rs.getString("CATEGORY_NAME"));
				rentalContentsRowData.setGenreName(rs.getString("GENRE_NAME"));
				rentalContentsRowData.setItemTitle(rs.getString("ITEM_NAME"));
				rentalContentsRowData.setItemId(rs.getInt("ITEM_ID"));
				rentalContentsRowData.setIdentificationNumber(rs.getInt("IDENTIFICATION_NUMBER"));
				rentalContentsRowData.setRentalDetailNumber(rs.getInt("RENTAL_DETAIL_NUMBER"));
				list.add(rentalContentsRowData);
			}

			return list;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return null;

	}

	public ArrayList<RentalDetailDTO> selectByRentalNumber(String rentalNumber) throws SQLException {

		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        rental.RENTAL_NUMBER");
		sb.append("        ,ORDER_DATETIME");
		sb.append("        ,date_add(");
		sb.append("            ORDER_DATETIME");
		sb.append("            ,INTERVAL 7 day");
		sb.append("        ) AS TO_RETURN_DATETIME");
		sb.append("        ,RETURN_DATETIME");
		sb.append("        ,(");
		sb.append("            SELECT");
		sb.append("                    COUNT(*)");
		sb.append("                FROM");
		sb.append("                    rental_detail");
		sb.append("                WHERE");
		sb.append("                    rental.RENTAL_NUMBER = rental_detail.RENTAL_NUMBER");
		sb.append("        ) AS COUNT");
		sb.append("        ,CONCAT(s1.LAST_NAME, ' ', s1.FIRST_NAME) AS RENTAL_STAFF_NAME");
		sb.append("        ,CONCAT(s2.LAST_NAME, ' ', s2.FIRST_NAME) AS RETURN_STAFF_NAME");
		sb.append("        ,rental.STATUS_ID");
		sb.append("        ,STATUS_NAME");
		sb.append("    FROM");
		sb.append("        rental");
		sb.append("            LEFT JOIN staff AS s1");
		sb.append("                ON rental.RENTAL_STAFF_ID = s1.STAFF_ID");
		sb.append("            LEFT JOIN staff AS s2");
		sb.append("                ON rental.RENTAL_STAFF_ID = s2.STAFF_ID");
		sb.append("            LEFT JOIN status");
		sb.append("                ON rental.STATUS_ID = status.STATUS_ID");
		sb.append("    WHERE");
		sb.append("        RENTAL_NUMBER = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// ?に値をセットして実行
			ps.setInt(1, Integer.parseInt(rentalNumber));

			// リストの作成
			ArrayList<RentalDetailDTO> list = new ArrayList<RentalDetailDTO>();
			// SQL文実行
			ResultSet rs = ps.executeQuery();

			// SQLの結果を取得し、リストに詰める
			while (rs.next()) {
				RentalDetailDTO rentalRowData = new RentalDetailDTO();
				rentalRowData.setRentalNumber(rs.getInt("RENTAL_NUMBER"));
				rentalRowData.setOrderDatetime(rs.getTimestamp("ORDER_DATETIME"));
				rentalRowData.setToReturnDatetime(rs.getDate("TO_RETURN_DATETIME"));
				rentalRowData.setReturnDatetime(rs.getDate("RETURN_DATETIME"));
				rentalRowData.setCount(rs.getInt("COUNT"));
				rentalRowData.setRentalStaffName(rs.getString("RENTAL_STAFF_NAME"));
				rentalRowData.setReturnStaffName(rs.getString("RETURN_STAFF_NAME"));
				rentalRowData.setStatusId(Integer.parseInt(rs.getString("STATUS_ID")));
				rentalRowData.setStatusName(rs.getString("STATUS_NAME"));
				list.add(rentalRowData);
			}

			return list;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return null;
	}

	/**
	 * あいまいAND検索を行える
	 * 
	 * @param rentalStaffId
	 * @param returnStaffId
	 * @param statusId
	 * @return 結果一覧(RentalDetailDTO)
	 * @throws SQLException
	 */
	public ArrayList<RentalDetailDTO> selectBySearch(String orderDate, String toReturnDate, String returnDate,
			String rentalStaffId, String returnStaffId, String statusId, int pageNum) throws SQLException {

		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        rental.RENTAL_NUMBER");
		sb.append("        ,ORDER_DATETIME");
		sb.append("        ,TO_RETURN_DATETIME");
		sb.append("        ,RETURN_DATETIME");
		sb.append("        ,(");
		sb.append("            SELECT");
		sb.append("                    COUNT(*)");
		sb.append("                FROM");
		sb.append("                    rental_detail");
		sb.append("                WHERE");
		sb.append("                    rental.RENTAL_NUMBER = rental_detail.RENTAL_NUMBER");
		sb.append("        ) AS COUNT");
		sb.append("        ,CONCAT(s1.LAST_NAME, ' ', s1.FIRST_NAME) AS RENTAL_STAFF_NAME");
		sb.append("        ,CONCAT(s2.LAST_NAME, ' ', s2.FIRST_NAME) AS RETURN_STAFF_NAME");
		sb.append("        ,rental.STATUS_ID");
		sb.append("        ,STATUS_NAME");
		sb.append("    FROM");
		sb.append("        rental");
		sb.append("            LEFT JOIN staff AS s1");
		sb.append("                ON rental.RENTAL_STAFF_ID = s1.STAFF_ID");
		sb.append("            LEFT JOIN staff AS s2");
		sb.append("                ON rental.RETURN_STAFF_ID = s2.STAFF_ID");
		sb.append("            LEFT JOIN status");
		sb.append("                ON rental.STATUS_ID = status.STATUS_ID");
		sb.append("    WHERE");
		sb.append("        (CASE WHEN ORDER_DATETIME IS NULL THEN '' ELSE DATE(ORDER_DATETIME) END) like ?");
		sb.append(
				"        AND (CASE WHEN TO_RETURN_DATETIME IS NULL THEN '' ELSE DATE(TO_RETURN_DATETIME) END) like ?");
		sb.append("        AND (CASE WHEN RETURN_DATETIME IS NULL THEN '' ELSE DATE(RETURN_DATETIME) END) like ?");
		sb.append("        AND (CASE WHEN RENTAL_STAFF_ID IS NULL THEN '' ELSE RENTAL_STAFF_ID END) like ?");
		sb.append("        AND (CASE WHEN RETURN_STAFF_ID IS NULL THEN '' ELSE RETURN_STAFF_ID END) like ?");
		sb.append("        AND rental.STATUS_ID like ?");
		sb.append("    LIMIT ? , 10");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// ?に値をセットして実行
			ps.setString(1, "%" + orderDate);
			ps.setString(2, "%" + toReturnDate);
			ps.setString(3, "%" + returnDate);
			ps.setString(4, "%" + rentalStaffId);
			ps.setString(5, "%" + returnStaffId);
			ps.setString(6, "%" + statusId);
			ps.setInt(7, pageNum * 10 - 10);

			// リストの作成
			ArrayList<RentalDetailDTO> list = new ArrayList<RentalDetailDTO>();
			// SQL文実行
			ResultSet rs = ps.executeQuery();

			// SQLの結果を取得し、リストに詰める
			while (rs.next()) {
				RentalDetailDTO rentalRowData = new RentalDetailDTO();
				rentalRowData.setRentalNumber(rs.getInt("RENTAL_NUMBER"));
				rentalRowData.setOrderDatetime(rs.getTimestamp("ORDER_DATETIME"));
				rentalRowData.setToReturnDatetime(rs.getDate("TO_RETURN_DATETIME"));
				rentalRowData.setReturnDatetime(rs.getDate("RETURN_DATETIME"));
				rentalRowData.setCount(rs.getInt("COUNT"));
				rentalRowData.setRentalStaffName(rs.getString("RENTAL_STAFF_NAME"));
				rentalRowData.setReturnStaffName(rs.getString("RETURN_STAFF_NAME"));
				rentalRowData.setStatusId(rs.getInt("STATUS_ID"));
				rentalRowData.setStatusName(rs.getString("STATUS_NAME"));
				list.add(rentalRowData);
			}

			return list;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return null;
	}

	public int selectCountContentsBySearch(String orderDate, String toReturnDate, String returnDate,
			String rentalStaffId, String returnStaffId, String statusId) throws SQLException {

		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUNT(*) AS COUNT");
		sb.append("    FROM");
		sb.append("        rental");
		sb.append("            LEFT JOIN staff AS s1");
		sb.append("                ON rental.RENTAL_STAFF_ID = s1.STAFF_ID");
		sb.append("            LEFT JOIN staff AS s2");
		sb.append("                ON rental.RETURN_STAFF_ID = s2.STAFF_ID");
		sb.append("            LEFT JOIN status");
		sb.append("                ON rental.STATUS_ID = status.STATUS_ID");
		sb.append("    WHERE");
		sb.append("        (");
		sb.append("            CASE");
		sb.append("                WHEN ORDER_DATETIME IS NULL THEN ''");
		sb.append("                ELSE DATE(ORDER_DATETIME)");
		sb.append("            END");
		sb.append("        ) LIKE ?");
		sb.append("        AND (");
		sb.append("            CASE");
		sb.append("                WHEN TO_RETURN_DATETIME IS NULL THEN ''");
		sb.append("                ELSE DATE(TO_RETURN_DATETIME)");
		sb.append("            END");
		sb.append("        ) LIKE ?");
		sb.append("        AND (");
		sb.append("            CASE");
		sb.append("                WHEN RETURN_DATETIME IS NULL THEN ''");
		sb.append("                ELSE DATE(RETURN_DATETIME)");
		sb.append("            END");
		sb.append("        ) LIKE ?");
		sb.append("        AND (");
		sb.append("            CASE");
		sb.append("                WHEN RENTAL_STAFF_ID IS NULL THEN ''");
		sb.append("                ELSE RENTAL_STAFF_ID");
		sb.append("            END");
		sb.append("        ) LIKE ?");
		sb.append("        AND (");
		sb.append("            CASE");
		sb.append("                WHEN RETURN_STAFF_ID IS NULL THEN ''");
		sb.append("                ELSE RETURN_STAFF_ID");
		sb.append("            END");
		sb.append("        ) LIKE ?");
		sb.append("        AND rental.STATUS_ID LIKE ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// ?に値をセットして実行
			ps.setString(1, "%" + orderDate);
			ps.setString(2, "%" + toReturnDate);
			ps.setString(3, "%" + returnDate);
			ps.setString(4, "%" + rentalStaffId);
			ps.setString(5, "%" + returnStaffId);
			ps.setString(6, "%" + statusId);

			// リストの作成
			ArrayList<RentalDetailDTO> list = new ArrayList<RentalDetailDTO>();
			// SQL文実行
			ResultSet rs = ps.executeQuery();

			int count = 0;
			// SQLの結果を取得し、リストに詰める
			while (rs.next()) {
				count = rs.getInt("COUNT");
			}

			return count;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return 0;
	}
}