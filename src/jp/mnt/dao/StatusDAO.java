package jp.mnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.mnt.dto.StatusDTO;

public class StatusDAO {
	Connection conn = null;

	public StatusDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	public ArrayList<StatusDTO> selectAll() throws SQLException {
		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        STATUS_ID");
		sb.append("        ,STATUS_NAME");
		sb.append("    FROM");
		sb.append("        STATUS;");


		// リストの作成
		ArrayList<StatusDTO> list = new ArrayList<StatusDTO>();
		// SQL文実行
		PreparedStatement ps = conn.prepareStatement(sb.toString());
		ResultSet rs = ps.executeQuery();

		// SQLの結果を取得し、リストに詰める
		while (rs.next()) {
			StatusDTO empRowData = new StatusDTO();
			empRowData.setStatusId(rs.getString("STATUS_ID"));
			empRowData.setStatusName(rs.getString("STATUS_NAME"));
			list.add(empRowData);
		}

		return list;
	}
}
