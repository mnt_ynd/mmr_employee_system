package jp.mnt.dto;

//新旧区分IDテーブル
public class NewAndOldDTO {
	private char newAndOldId;
	private String newAndOldName;
	public char getNewAndOldId() {
		return newAndOldId;
	}
	public void setNewAndOldId(char newAndOldId) {
		this.newAndOldId = newAndOldId;
	}
	public String getNewAndOldName() {
		return newAndOldName;
	}
	public void setNewAndOldName(String newAndOldName) {
		this.newAndOldName = newAndOldName;
	}
	
	
}
