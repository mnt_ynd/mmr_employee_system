package jp.mnt.dto;

//お届け日時テーブル
public class DeliveryTimeDTO {
	private char deliveryTimeId;
	private String deliveryTimeName;
	
	public char getDeliveryTimeId() {
		return deliveryTimeId;
	}
	public void setDeliveryTimeId(char deliveryTimeId) {
		this.deliveryTimeId = deliveryTimeId;
	}
	public String getDeliveryTimeName() {
		return deliveryTimeName;
	}
	public void setDeliveryTimeName(String deliveryTimeName) {
		this.deliveryTimeName = deliveryTimeName;
	}
	
	
}
