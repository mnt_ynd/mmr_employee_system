package jp.mnt.dto;

//配布クーポンテーブル
public class CouponDistributionDTO {
	private int couponDistributionIndex;
	private int couponId;
	private int userIndex;
	
	public int getCouponDistributionIndex() {
		return couponDistributionIndex;
	}
	public void setCouponDistributionIndex(int couponDistributionIndex) {
		this.couponDistributionIndex = couponDistributionIndex;
	}
	public int getCouponId() {
		return couponId;
	}
	public void setCouponId(int couponId) {
		this.couponId = couponId;
	}
	public int getUserIndex() {
		return userIndex;
	}
	public void setUserIndex(int userIndex) {
		this.userIndex = userIndex;
	}
	
	
}
