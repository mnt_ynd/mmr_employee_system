package jp.mnt;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.dao.AnalyticsDAO;

/**
 * Servlet implementation class analyticsServlet
 */
@WebServlet("/analytics")
public class analyticsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッションの取得
		// HttpSession session = request.getSession(false);
		// TODO 結合後にログイン判定を導入
		HttpSession session = request.getSession(true);
		session.setAttribute("staff_id", "user1");
		session.setAttribute("manager_flg", 1);

		// ログインしていなければログインに遷移
		String staffId = (String) session.getAttribute("staff_id");
		if (staffId == null) {
			response.sendRedirect("login");
		}

		// 管理者でなければレンタル検索ページへ
		String managerFLG = (String) session.getAttribute("staff_id");
		if (managerFLG == null) {
			response.sendRedirect("rental");
		}

		// 分析機能
		try (Connection conn = DataSourceManager.getConnection()) {

			// DAO
			AnalyticsDAO analyticsDAO = new AnalyticsDAO(conn);

			// 先月の年代別利用者数
			int[] thisYear = analyticsDAO.thisYear();

			// 去年の先月の年代別利用者数
			int[] lastYear = analyticsDAO.lastYear();

			// グラフ用にデータを作成
			int[] data = new int[7];
			for (int i = 0; i < 7; i++) {
				data[i] = thisYear[i] - lastYear[i];
			}

			// リクエストスコープに設定
			request.setAttribute("this_year", thisYear);
			request.setAttribute("last_year", lastYear);
			request.setAttribute("data", data);

			// analyticsへ遷移
			request.getRequestDispatcher("/WEB-INF/jsp/analyticsView.jsp").forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
