package jp.mnt.staff;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.Connection;

import jp.mnt.DataSourceManager;
import jp.mnt.dao.PasswordDAO;

/**
 * Servlet implementation class PasswordChangeServlet
 */
@WebServlet("/passwordChange")
public class PasswordChangeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("login.jsp");
	}

	protected void doGet2(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("doget2");
		request.getRequestDispatcher("/WEB-INF/jsp/passwordChange.jsp").forward(request, response);
		
		
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession(false);
		if(session == null){
			doGet(request, response);
			return;
		}
		
		String password1 = request.getParameter("pass1");
		String password2 = request.getParameter("pass2");
		String id = (String)session.getAttribute("id");
		System.out.println(password1);
		System.out.println(password2);
		System.out.println(id);

		// password1とpassword2の入力チェックを行う
		String check = passwordCheck(password1, password2);
		if (!check.equals("")) {
			session.setAttribute("error_message", check);
			doGet2(request, response);
			return;
		}

		// コネクションの取得
		
		Connection conn = null;
		try {
			conn = (Connection) DataSourceManager.getConnection();

			PasswordDAO dao = new PasswordDAO(conn);
			dao.updatePassword(id, password1);
			

			// trueの時error_messageをリセット
			session.removeAttribute("error_message");

			// trueの時RentalServletにフォワード
			request.getRequestDispatcher("rental").forward(request, response);

		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO: handle exception
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * パスワードとパスワード（確認）の入力チェックを行う
	 */
	public String passwordCheck(String pass1, String pass2) {

		String check = "";

		// pass1に半角英数字以外があるとエラー文を返す
		for (int i = 0; i < pass1.length(); i++) {
			char c = pass1.charAt(i);
			if ((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z')) {
				check = "パスワードには半角英数字のみ入力してください";
				return check;
			}
		}

		// pass2に半角英数字以外があるとエラー文を返す
		for (int i = 0; i < pass2.length(); i++) {
			char c = pass2.charAt(i);
			if ((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z')) {
				check = "パスワードには半角英数字のみ入力してください";
				return check;
			}
		}

		// pasu1とpass2が違うとエラー文を返す
		if (!pass1.equals(pass2)) {
			check = "パスワードとパスワード（確認）が違います";
			return check;
		}

		return check;
	}

}
