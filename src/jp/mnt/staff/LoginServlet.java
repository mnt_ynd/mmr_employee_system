package jp.mnt.staff;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.DataSourceManager;
import jp.mnt.dao.StaffDAO;

/**
 * login.jspからsubmitされた値をチェックして遷移する
 */

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.sendRedirect("login.jsp");
	}
	
	

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession(false);
		String id = request.getParameter("id");
		session.setAttribute("id", id);
		String password = request.getParameter("pass");
		

		// 入力チェックを行い不適切だとerror_messageをlogiin.jspに送る
		String check = loginCheck(id, password);
		if (!check.equals("")) {
			session.setAttribute("error_message", check);
			doGet(request, response);
			return;
		}

		// コネクションの取得
		
		Connection conn = null;
		try {
			conn = DataSourceManager.getConnection();

			// idとpasswordの検索
			StaffDAO dao = new StaffDAO(conn);
			boolean selectByIdAndPassword = false;
			selectByIdAndPassword = dao.selectByIdAndPassword(id, password);

			System.out.println(selectByIdAndPassword);

			// falseの時error_messageをlogiin.jspに送る
			if (selectByIdAndPassword == false) {
				session.setAttribute("error_message", "ユーザIDまたはパスワードが間違っています");
				doGet(request, response);
				return;
			}
			

			// trueの時error_messageをリセット
			session.removeAttribute("error_message");
			
			//パスワードが初期値(staff)の時パスワード変更画面に遷移する
			if(password.equals("staff")){
				System.out.println("change");
				request.getRequestDispatcher("/WEB-INF/jsp/passwordChange.jsp").forward(request, response);
				return;
			}

			// session.setAttribute("id", "true");

			// trueの時RentalServletにフォワード
			request.getRequestDispatcher("rental").forward(request, response);

		} catch (SQLException e) {
			
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			
			e.printStackTrace();
		}

	}

	/**
	 * ログインIDとパスワードの入力チェックを行う
	 */
	public String loginCheck(String id, String pass) {

		boolean b = true;
		String check = "";

		// idが数字以外があるとエラー文を返す
		try {
			Integer.parseInt(id);
			b = true;
		} catch (Exception e) {
			b = false;
		}
		if (b == false) {
			check = "ログインIDには半角数字のみ入力してください";
			return check;
		}

		// passに半角英数字以外があるとエラー文を返す
		for (int i = 0; i < pass.length(); i++) {
			char c = pass.charAt(i);
			if ((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z')) {
				check = "パスワードには半角英数字のみ入力してください";
				return check;
			}
		}
		return check;
	}

}
