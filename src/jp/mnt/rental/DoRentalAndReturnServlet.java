package jp.mnt.rental;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.DataSourceManager;
import jp.mnt.dao.RentalDAO;
import jp.mnt.dao.RentalDetailDAO;
import jp.mnt.dto.RentalDetailDTO;

/**
 * Servlet implementation class DoRentalAndReturnServlet
 */
@WebServlet("/doRentalAndReturn")
public class DoRentalAndReturnServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッションの取得
		// HttpSession session = request.getSession(false);
		// TODO 結合後に消す
		HttpSession session = request.getSession(true);
		session.setAttribute("staff_id", "1");
		session.setAttribute("managerFLG", 1);

		// ログインしていなければログインに遷移
		String userName = (String) session.getAttribute("staff_id");
		if (userName == null) {
			response.sendRedirect("login");
		}

		// 値を受け取る
		request.setCharacterEncoding("UTF-8");
		String whatProcess = request.getParameter("whatProcess");

		// 変数に格納
		String rentalNumberStr = (String) session.getAttribute("rental_Number");
		String staffIdStr = (String) session.getAttribute("staff_id");

		// TODO Vaildate
		int rentalNumber = Integer.parseInt(rentalNumberStr);
		int staffId = Integer.parseInt(staffIdStr);

		// 貸出処理
		// TODO 個体識別番号が存在するかどうか(最大値、廃盤）
		if ("rental".equals(whatProcess)) {
			try (Connection conn = DataSourceManager.getConnection()) {
				RentalDAO rentalDao = new RentalDAO(conn);

				// レンタル内容
				RentalDetailDAO rentalDetailDao = new RentalDetailDAO(conn);
				ArrayList<RentalDetailDTO> rentalContentsList = rentalDetailDao
						.selectContentsByRentalNumber(rentalNumberStr);

				// トランザクション(手動コミット)
				conn.setAutoCommit(false);
				int error = 0;

				// 個体識別番号をアップデート
				for (RentalDetailDTO rentalDetailDTO : rentalContentsList) {
					int rentalDetailNumber = rentalDetailDTO.getRentalDetailNumber();

					String identificationNumberStr = request.getParameter("item_" + rentalDetailNumber);
					int identificationNumber = Integer.parseInt(identificationNumberStr);

					// 個体識別番号を設定
					int setIdentificationResult = rentalDao.setIdentificationNumBer(rentalDetailNumber,
							identificationNumber);
					if (setIdentificationResult != 1) {
						error = 1;
					}
				}

				// ステータスを貸出に変更
				int rentalResult = rentalDao.doRental(staffId, rentalNumber);

				if (rentalResult != 1) {
					error = 1;
				}

				if (error != 1) {
					conn.commit();
					request.setAttribute("rental_result_message", "貸出が成功しました。");
				} else {
					conn.rollback();
					request.setAttribute("rental_result_message", "貸出に失敗しました。");
				}

				// トランザクション(オートコミット)を戻す
				conn.setAutoCommit(true);

				// 遷移する
				request.getRequestDispatcher("/WEB-INF/jsp/rentalView.jsp").forward(request, response);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// 返却処理
		if ("return".equals(whatProcess)) {
			try (Connection conn = DataSourceManager.getConnection()) {

				// レンタル詳細を取得
				RentalDAO rentalDao = new RentalDAO(conn);
				int rentalResult = rentalDao.doReturn(staffId, rentalNumber);

				if (rentalResult == 1) {
					request.setAttribute("return_result_message", "返却処理完了");
				} else {
					request.setAttribute("return_result_message", "返却処理に失敗しました。");
				}
				// 遷移する
				request.getRequestDispatcher("/WEB-INF/jsp/rentalView.jsp").forward(request, response);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// クエリパラメータが不正な値だった場合の処理

	}

	public String isNum(String num) {
		if (num.length() == 0) {
			return "";
		}
		try {
			Integer.parseInt(num);
			return num;
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
