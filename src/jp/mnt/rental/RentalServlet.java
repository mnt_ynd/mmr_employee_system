package jp.mnt.rental;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.DataSourceManager;
import jp.mnt.dao.RentalDetailDAO;
import jp.mnt.dao.StaffDAO;
import jp.mnt.dao.StatusDAO;
import jp.mnt.dto.RentalDetailDTO;
import jp.mnt.dto.StaffDTO;
import jp.mnt.dto.StatusDTO;

/**
 * レンタル情報の検索
 * 
 * @author ソフトブレーン富岡
 *
 */
@WebServlet("/rental")
public class RentalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッションの取得
		// HttpSession session = request.getSession(false);
		// TODO 結合後にログイン判定を導入
		HttpSession session = request.getSession(true);
		session.setAttribute("staff_id", "user1");
		session.setAttribute("manager_flg", 1);

		// ログインしていなければログインに遷移
		String userName = (String) session.getAttribute("staff_id");
		if (userName == null) {
			response.sendRedirect("login");
		}

		try (Connection conn = DataSourceManager.getConnection()) {

			// 従業員一覧を取得(検索用セレクトボックス)
			StaffDAO staffDao = new StaffDAO(conn);
			ArrayList<StaffDTO> staffList = staffDao.selectAll();

			// ステータステーブルの値を取得(検索用セレクトボックス)
			StatusDAO statudDao = new StatusDAO(conn);
			ArrayList<StatusDTO> statusList = statudDao.selectAll();

			// レンタル詳細情報を検索
			RentalDetailDAO rentalDetailDao = new RentalDetailDAO(conn);
			ArrayList<RentalDetailDTO> rentalDetailList = null;

			// レンタル一覧
			// RentalDAO rentalDao = new RentalDAO(conn);

			// 検索ボタン以外から来た
			if (request.getParameter("page") == null) {
				request.setAttribute("home", 1);
			}

			// 検索ボタンから
			String orderDate = request.getParameter("orderDate");
			String toReturnDate = request.getParameter("toReturnDate");
			String returnDate = request.getParameter("returnDate");
			String rentalStaffId = validetaNum(request.getParameter("rentalStaffId"));
			String returnStaffId = validetaNum(request.getParameter("returnStaffId"));
			String statusId = validetaNum(request.getParameter("statusId"));
			String[] inputStr = { orderDate, toReturnDate, returnDate, rentalStaffId, returnStaffId, statusId };

			int countSearch = rentalDetailDao.selectCountContentsBySearch(orderDate, toReturnDate, returnDate,
					rentalStaffId, returnStaffId, statusId);

			// ページング数を格納
			if (countSearch == 0) {
			} else if (countSearch % 10 == 0) {
				request.setAttribute("page", countSearch / 10);
			} else {
				request.setAttribute("page", countSearch / 10 + 1);
			}

			// 現在のページを取得
			String currentPage = request.getParameter("page");
			int pageNumber = 1;
			if (currentPage == null) {
				pageNumber = 1;
			} else if (validetaNum(currentPage).length() >= 1) {
				pageNumber = Integer.parseInt(currentPage);
			}

			// 表示するレンタル概要の検索
			rentalDetailList = rentalDetailDao.selectBySearch(orderDate, toReturnDate, returnDate, rentalStaffId,
					returnStaffId, statusId,pageNumber);

			// リクエストスコープにデータを設定する
			request.setAttribute("input_str", inputStr);
			request.setAttribute("staff_list", staffList);
			request.setAttribute("status_list", statusList);
			request.setAttribute("rental_list", rentalDetailList);
			request.setAttribute("current_page", pageNumber);

			// rentalサーブレットへ
			request.getRequestDispatcher("/WEB-INF/jsp/rentalView.jsp").forward(request, response);
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String validetaNum(String num) {
		try {
			Integer.parseInt(num);
			return num;
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
