package jp.mnt.rental;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.DataSourceManager;
import jp.mnt.dao.RentalDetailDAO;
import jp.mnt.dto.RentalDetailDTO;

/**
 * Servlet implementation class RentalDetailServlet
 */
@WebServlet("/rentalDetail")
public class RentalDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッションの取得
		// HttpSession session = request.getSession(false);
		// TODO 結合後に消す
		HttpSession session = request.getSession(true);
		session.setAttribute("staff_id", "user1");
		session.setAttribute("managerFLG", 1);

		// ログインしていなければログインに遷移
		String userName = (String) session.getAttribute("staff_id");
		if (userName == null) {
			response.sendRedirect("login");
		}

		// URLクエリパラメータを取得
		String rentalNumber = request.getParameter("rentalNumber");

		// 入力値が正しくなければエラーページへ
		if (isEmpno(rentalNumber) == false) {
			response.sendRedirect("rental");
		}

		try (Connection conn = DataSourceManager.getConnection()){

			// レンタル詳細を取得
			RentalDetailDAO rentalDetailDao = new RentalDetailDAO(conn);
			ArrayList<RentalDetailDTO> rentalDetailList = rentalDetailDao.selectByRentalNumber(rentalNumber);

			// レンタル内容を取得
			ArrayList<RentalDetailDTO> rentalContentsList = rentalDetailDao.selectContentsByRentalNumber(rentalNumber);

			// リクエストスコープにデータを設定する
			session.setAttribute("rental_Number", rentalNumber);
			request.setAttribute("rental_Detail_List", rentalDetailList);
			request.setAttribute("rental_Contens_List", rentalContentsList);

			// 遷移する
			request.getRequestDispatcher("/WEB-INF/jsp/rentalDetailView.jsp").forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	public boolean isEmpno(String rentalNumber) {
		if (rentalNumber == null) {
			return false;
		}
		try {
			Integer.parseInt(rentalNumber);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}
