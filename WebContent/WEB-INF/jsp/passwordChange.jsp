<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>パスワード設定</title>
<script type="text/javascript">
	function passwordChange() {
		if(fm.pass1.value == "" || fm.pass2.value == ""){
			alert("パスワードとパスワード(確認)を入力してください");
			return false;
		}
		return true;
	}
</script>
</head>
<body>

<%if(null != session.getAttribute("error_message")) {%>
	<div style="color: red">
	<%= session.getAttribute("error_message") %></div>
	<%} %>
	<%session.removeAttribute("error_message"); %>
	
	<h4>パスワードの設定が必要です。パスワードを設定してください</h4>
	<form name="fm" action="passwordChange" method="post">
		パスワード　　　　 <input type="password" name="pass1"><br> 
		パスワード（確認）<input type="password" name="pass2"><br>
		<input type="submit" value="登録" onclick="return passwordChange()">
	</form>
</body>
</html>