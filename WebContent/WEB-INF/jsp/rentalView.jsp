<%@page import="java.sql.Timestamp"%>
<%@ page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="jp.mnt.dto.StaffDTO,jp.mnt.dto.RentalDetailDTO,java.util.ArrayList,java.util.Date"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-language" content="ja">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>レンタル一覧 -MMR従業員システム-</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>HOME</title>
<!-- bootstrap3 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<!-- jQuery 3.1 -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">

<!-- CSS -->
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<a class="navbar-brand" href="rental"><img src="image/logo.png" width="100px" /></a>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<c:if test="${manager_flg == 1}">
					<li class="nav-item"><a class="nav-link" href="itemMaintenance">商品メンテ <span class="sr-only">(current)</span></a></li>
					<li class="nav-item"><a class="nav-link" href="analytics">分析 <span class="sr-only">(current)</span></a></li>
				</c:if>
				<li class="nav-item"><a class="nav-link" href="logout">ログアウト <span class="sr-only">(current)</span></a></li>
			</ul>
		</div>
	</nav>
	<%-- エラーメッセージがあれば表示する --%>
	<!-- 貸出メッセージ -->
	<c:if test="${!empty rental_result_message}">
		<div class="container">
			<div style="color: red">
				<c:out value="${rental_result_message}" />
			</div>
		</div>
	</c:if>
	<!-- 返却メッセージ -->
	<c:if test="${!empty return_result_message}">
		<div class="container">
			<div style="color: red">
				<c:out value="${return_result_message}" />
			</div>
		</div>
	</c:if>
	<div class="container">
		<h2>検索</h2>
		<form name="serch" action="rental" method="get">
			<input type="hidden" name="page" value="1" />
			<div class="row">
				<div class="col-sm-12 col-md-7">
					<div class="row">
						<div class="col-sm-4 col-md-4">
							<label>受付日</label>
							<br>
							<input type="date" name="orderDate" value="<c:out value="${requestScope.input_str[0]}" />" />
						</div>
						<div class="col-sm-4 col-md-4">
							<label>返却予定日</label>
							<br>
							<input type="date" name="toReturnDate" value="<c:out value="${requestScope.input_str[1]}" />" />
						</div>
						<div class="col-sm-4 col-md-4">
							<label>返却日</label>
							<br>
							<input type="date" name="returnDate" value="<c:out value="${requestScope.input_str[2]}" />" />
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-4">
					<div class="row">
						<div class="col-sm-4 col-md-4">
							<label>貸出担当者</label>
							<br>
							<select name="rentalStaffId">
								<option value="">- 全て -</option>
								<c:forEach var="dto" items="${requestScope.staff_list}">
									<c:choose>
										<c:when test="${dto.staffId == requestScope.input_str[3]}">
											<option value="${dto.staffId}" selected><c:out
													value="${dto.lastName} ${dto.firstName}" /></option>
										</c:when>
										<c:otherwise>
											<option value="${dto.staffId}"><c:out value="${dto.lastName} ${dto.firstName}" /></option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
						<div class="col-sm-4 col-md-4">
							<label>返却担当者</label>
							<br>
							<select name="returnStaffId">
								<option value="">- 全て -</option>
								<c:forEach var="dto" items="${requestScope.staff_list}">
									<c:choose>
										<c:when test="${dto.staffId == requestScope.input_str[4]}">
											<option value="${dto.staffId}" selected><c:out
													value="${dto.lastName} ${dto.firstName}" /></option>
										</c:when>
										<c:otherwise>
											<option value="${dto.staffId}"><c:out value="${dto.lastName} ${dto.firstName}" /></option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
						<div class="col-sm-4 col-md-4">
							<label>ステータス</label>
							<br>
							<select name="statusId">
								<option value="">- 全て -</option>
								<c:forEach var="dto" items="${requestScope.status_list}">
									<c:choose>
										<c:when test="${dto.statusId == requestScope.input_str[5]}">
											<option value="${dto.statusId}" selected><c:out value="${dto.statusName}" /></option>
										</c:when>
										<c:otherwise>
											<option value="${dto.statusId}"><c:out value="${dto.statusName}" /></option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-1">
					<div class="col-sm-3 col-md-1">
						<input type="submit" value="検索" />
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="container">
		<h2>検索結果一覧</h2>
		<div class="row">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>受付番号</th>
						<th>受付時間</th>
						<th>返却予定日</th>
						<th>返却日</th>
						<th>数量</th>
						<th>貸出担当</th>
						<th>返却担当</th>
						<th>ステータス</th>
						<th>詳細</th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${home == 1}">
							<tr>
								<td colspan="8">検索を実行してください</td>
							</tr>
							<c:remove var="home" scope="request" />
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${requestScope.rental_list.size() == 0 }">
									<tr>
										<td colspan="8">データがありません。</td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:forEach var="dto" items="${requestScope.rental_list}">
										<tr>
											<th scope="row"><c:out value="${dto.rentalNumber}" /></th>
											<td><c:out value="${fn:substringAfter(dto.orderDatetime , ' ' ) }" /></td>
											<td><c:out value="${dto.toReturnDatetime}" /></td>
											<td><c:out value="${dto.returnDatetime}" /></td>
											<td><c:out value="${dto.count}" /></td>
											<td><c:out value="${dto.rentalStaffName}" /></td>
											<td><c:out value="${dto.returnStaffName}" /></td>
											<c:choose>
												<c:when test="${dto.statusId == 4 }">
													<td class="bg-danger"><c:out value="${dto.statusName}" /></td>
												</c:when>
												<c:otherwise>
													<td><c:out value="${dto.statusName}" /></td>
												</c:otherwise>
											</c:choose>
											<td><a href="rentalDetail?rentalNumber=${dto.rentalNumber}"><button
														type="button">詳細</button></a></td>
										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
			<!-- ページング終わり -->
			<div id="page">
				<c:if test="${!empty page}">
					<ul class="pageNav">
						<c:forEach begin="1" end="${page}" step="1" varStatus="stat">
							<c:choose>
								<c:when test="${stat.index == current_page}">
									<li style="background: black;"><span> <c:out value="${stat.index}" /></span></li>
								</c:when>
								<c:otherwise>
									<li><a
										href="rental?page=<c:out value="${stat.index}" />&orderDate=<c:out value="${requestScope.input_str[0]}" />&toReturnDate=<c:out value="${requestScope.input_str[1]}" />&returnDate=<c:out value="${requestScope.input_str[3]}" />&rentalStaffId=<c:out value="${requestScope.input_str[4]}" />&returnStaffId=<c:out value="${requestScope.input_str[5]}" />&statusId=<c:out value="${requestScope.input_str[6]}" />"><c:out
												value="${stat.index}" /></a></li>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</ul>
				</c:if>
			</div><!-- ページング終わり -->
		</div>

	</div>

	<footer class="footer">
		<div class="container">
			<p class="text-muted text-center">copyright © 2017 music &amp; movie rental Co., Ltd All
				rights reserved.</p>
		</div>
	</footer>
</body>
</html>