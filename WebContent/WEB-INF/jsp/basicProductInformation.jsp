<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="jp.mnt.dto.ItemDTO,java.util.ArrayList" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>商品マスタメンテナンス</title>
<!-- bootstrap3 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<!-- jQuery 3.1 -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">

<!-- CSS -->
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

	
	<script type="text/javascript" src="select.js" charset="Shift_JIS"></script>
	<script type="text/javascript">

	var box1 = new SelectBox("sb1");
	box1.registOption(new SelectOption(null, "カテゴリ", "0", "color:gray;"));
	box1.registOption(new SelectOption(null, "CD"   , "1"));
	box1.registOption(new SelectOption(null, "DVD"  , "2"));
	box1.registOption(new SelectOption(null, "BD"   , "3"));

	var box2 = new SelectBox("sb2");
	box2.registOption(new SelectOption(null, "ジャンル", "0", "color:gray;"));
	box2.registOption(new SelectOption("1" , "J-POP", "1"));
	box2.registOption(new SelectOption("1" , "ロック＆ポップス", "2"));
	box2.registOption(new SelectOption("1" , "ブラック/ソウル", "3"));
	box2.registOption(new SelectOption("1" , "ヒップホップ/ラップ", "4"));
	box2.registOption(new SelectOption("1" , "クラブ/ダンス", "5"));
	box2.registOption(new SelectOption("1" , "ハードロック/メタル", "6"));
	box2.registOption(new SelectOption("1" , "レゲエ", "7"));
	box2.registOption(new SelectOption("1" , "ワールド", "8"));
	box2.registOption(new SelectOption("1" , "邦楽JAZZ", "9"));
	box2.registOption(new SelectOption("1" , "洋楽JAZZ", "10"));
	box2.registOption(new SelectOption("1" , "フュージョン", "11"));
	box2.registOption(new SelectOption("1" , "ブルース", "12"));
	box2.registOption(new SelectOption("1" , "アニメ/ゲーム", "13"));
	box2.registOption(new SelectOption("1" , "ネット/ボーカロイド", "14"));
	box2.registOption(new SelectOption("1" , "童謡", "15"));
	box2.registOption(new SelectOption("1" , "キッズ", "16"));
	box2.registOption(new SelectOption("1" , "演歌/民謡", "17"));
	box2.registOption(new SelectOption("1" , "邦画サントラ", "18"));
	box2.registOption(new SelectOption("1" , "洋画サントラ", "19"));
	box2.registOption(new SelectOption("1" , "クラッシック", "20"));
	box2.registOption(new SelectOption("1" , "イージーリスニング", "21"));
	box2.registOption(new SelectOption("1" , "バラエティ", "22"));
	box2.registOption(new SelectOption("1" , "その他", "23"));
	box2.registOption(new SelectOption("2" , "洋画", "24"));
	box2.registOption(new SelectOption("2" , "邦画", "25"));
	box2.registOption(new SelectOption("2" , "アニメ", "26"));
	box2.registOption(new SelectOption("2" , "スポーツ", "27"));
	box2.registOption(new SelectOption("2" , "ミュージック", "28"));
	box2.registOption(new SelectOption("2" , "HOWTO", "29"));
	box2.registOption(new SelectOption("2" , "アジアTVドラマ", "30"));
	box2.registOption(new SelectOption("2" , "海外TVドラマ", "31"));
	box2.registOption(new SelectOption("2" , "その他", "32"));
	box2.registOption(new SelectOption("3" , "洋画", "33"));
	box2.registOption(new SelectOption("3" , "邦画", "34"));
	box2.registOption(new SelectOption("3" , "アニメ", "35"));
	box2.registOption(new SelectOption("3" , "スポーツ", "36"));
	box2.registOption(new SelectOption("3" , "ミュージック", "37"));
	box2.registOption(new SelectOption("3" , "HOWTO", "38"));
	box2.registOption(new SelectOption("3" , "アジアTVドラマ", "39"));
	box2.registOption(new SelectOption("3" , "海外TVドラマ", "40"));
	box2.registOption(new SelectOption("3" , "その他", "41"));

	
	box1.setChild(box2);
	

	window.onload = function() {
	  box1.make(null);
	};
	function submitHandler(detail) {
	    var form = document.forms["detailForm"];
	   
	    form.detail.value = detail;
	    form.submit();

	}
	</script>
	<script type="text/javascript" src="recommded.js">
	
	</script>
	<body>
	<jsp:include page="menu.jsp" />
	<br>
	<div class="container">
	<h1>商品マスタメンテナンス</h1>
	<form name="add" action="itemDetail" method="post" ><input name="add" type="hidden" value="add"><input type="submit" value="商品追加"></form>
	</div>
	<div class="container">
	<h4>商品検索</h4>
	
	
	<form name="search" action="basicProductInformation" method="post">
	<select name="category" id="sb1" onchange="box2.make(this.value);"></select>
	<select name="genre" id="sb2" onchange="box3.make(this.value);"></select>
	<input type="text" name="item"> 
	<input type="submit" value="検索">
	</form>
	</div>
	<br>
	<div class="container">
	<h4>検索結果</h4>
	</div>
	<br>
	<%if(request.getAttribute("message") != null){ %>
		
		
		<%if( request.getAttribute("message").equals("")){%>
			<div class="container">
			<table class="table" >
			
			
			<%String eq = ""; %>
			
			
			<% for(ItemDTO dto : (ArrayList<ItemDTO>)request.getAttribute("ItemList")){ %>
			
			<tr>
				<div class="panel panel-default">
				<div class="panel-body">
				<div class="row">
				<form name="detailForm" action="itemDetail" method="post"><div class="col-md-2"><input type="hidden" name="detail" value=""><img onclick="submitHandler(<%= dto.getItemId()%>);" style="width: 130px; height: 80px" src="getimage?id=<%= dto.getItemId()%>" onerror="src='/MMREmployeeSystem/image/no_image.png'"></div></form>
				<div class="col-md-9">
				<div class="row">
				<div class="col-md-4"><%= dto.getItemId() %></div>
				<div class="col-md-4"><%= dto.getNewAndOldName() %></div>
				<div class="col-md-4"><%= dto.getCategoryName() %></div>
				
				
				
				<div class="col-md-12">
				
				<a href="#" onclick="submitHandler(<%= dto.getItemId()%>);">
				<%= dto.getItemName() %>
				</a>
				</div>
				
				
				
				<div class="col-md-4"><%= dto.getArtist()%></div>
				<div class="col-md-4"><%= dto.getPrice() %>円</div>
				<div class="col-md-4"><input type="button" onclick="submitHandler(<%= dto.getItemId()%>);" value="詳細へ"></div>
				</div>
				</div>
				<div class="col-md-1">
				
				<form action="recommended" name="form1" method="post">
					<%if(dto.getRecommendedFlg() == 0){ %>
					<input type="hidden" name="i" value="<%= dto.getItemId()%>">
					<input type="hidden" name="r" value="<%= dto.getRecommendedFlg()%>">
					<input type="submit" value="おススメ追加">
					<%} else{%>
					<input type="hidden" name="i" value="<%= dto.getItemId()%>">
					<input type="hidden" name="r" value="<%= dto.getRecommendedFlg()%>">
					<input type="submit" value="おススメ解除">
					<%} %>
				</form>
				
				
				</div>
				</div>
				</div>
				</div>
			</tr>
			
			<!-- ページング終わり -->
			<div id="page">
				<c:if test="${!empty page}">
					<ul class="pageNav">
						<c:forEach begin="1" end="${page}" step="1" varStatus="stat">
							<c:choose>
								<c:when test="${stat.index == current_page}">
									<li style="background: black;"><span> <c:out value="${stat.index}" /></span></li>
								</c:when>
								<c:otherwise>
									<!-- 機能ごとに変更 -->
									<li><a
										href="rental?page=<c:out value="${stat.index}" />&orderDate=<c:out value="${requestScope.input_str[0]}" />&toReturnDate=<c:out value="${requestScope.input_str[1]}" />&returnDate=<c:out value="${requestScope.input_str[3]}" />&rentalStaffId=<c:out value="${requestScope.input_str[4]}" />&returnStaffId=<c:out value="${requestScope.input_str[5]}" />&statusId=<c:out value="${requestScope.input_str[6]}" />">
										<c:out value="${stat.index}" /></a></li>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</ul>
				</c:if>
			</div><!-- ページング終わり -->
			<%}%>
			
			
			
		
		</table>
		</div>
		<%}else{%>  
			<div><%= (String)request.getAttribute("message") %></div>
			
		<%}%>
	<%} %>
		
<jsp:include page="footer.jsp" />
</body>
</html>