<%@page import="java.sql.Timestamp"%>
<%@ page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="jp.mnt.dto.StaffDTO,jp.mnt.dto.RentalDetailDTO,java.util.ArrayList,java.util.Date"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-language" content="ja">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>分析 -MMR従業員システム-</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>HOME</title>
<!-- bootstrap3 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<!-- jQuery 3.1 -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">

<!-- CSS -->
<link rel="stylesheet" type="text/css" href="css/style.css">

<!-- グラフライブラリ -->
<script src="js/Chart.js"></script>
</head>
<body>
	<!-- メニューバー -->
	<jsp:include page="menu.jsp" />
	<div class="container">
		<h2>分析</h2>
		<div class="row">
			<div class="col-md-12">前月における昨年同月との利用者数の比較</div>
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<!-- グラフ -->
				<canvas id="myChart"></canvas>
			</div>
			<div class="col-md-1"></div>
			<!-- グラフデータ -->
			<script>
			<!--
				var ctx = document.getElementById('myChart').getContext('2d');
				var myChart = new Chart(ctx, {
					type : 'bar',
					data : {
						labels : [ "10代以下", "20代", "30代", "40代", "50代", "60代", "70代以上"],

						datasets : [ {
							label : '利用者増加数(人)',
							data : [
								${requestScope.data[0]},
								${requestScope.data[1]},
								${requestScope.data[2]},
								${requestScope.data[3]},
								${requestScope.data[4]},
								${requestScope.data[5]},
								${requestScope.data[6]}
							],
							backgroundColor :[
								"rgba(255,215,0,0.4)",
								"rgba(255,69,0,0.4)",
								"rgba(100,255,51,0.4)",
								"rgba(176,196,222,0.4)",
								"rgba(184,183,107,0.4)",
								"rgba(219,112,147,0.4)",
								"rgba(199,21,133,0.4)"
							]
						} ]
					}
				});
				-->
			</script>
		</div>
	</div>
	<jsp:include page="footer.jsp" />
</body>
</html>