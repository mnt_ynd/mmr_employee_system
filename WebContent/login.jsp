<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ログイン</title>
<script type="text/javascript">
	function login() {
		if(fm.id.value == "" || fm.pass.value == ""){
			alert("ログインIDとパスワードを入力してください");
			return false;
		}
		return true;
	}
</script>
</head>
<body>
	<%if(null != session.getAttribute("error_message")) {%>
	<div style="color: red">
	<%= session.getAttribute("error_message") %></div>
	<%} %>
	<%session.removeAttribute("error_message"); %>
	
	<h1>従業員・管理者ログイン</h1>
	<form name="fm" action="login" method="post">
		ログインID<input type="text" name="id"><br> 
		パスワード<input type="password" name="pass"><br>
		<input type="submit" value="ログイン" onclick="return login()">

	</form>
</body>
</html>